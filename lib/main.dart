import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: LocationApp(),
    );
  }
}
class LocationApp extends StatefulWidget {
  const LocationApp({Key? key}) : super(key: key);

  @override
  _LocationAppState createState() => _LocationAppState();
}

class _LocationAppState extends State<LocationApp> {
  var locationMsg = "";
  var i = 0;
  Timer? timer;
  bool startBtnStatus = true;
  bool stopBtnStatus = false;

  void getCurrentLocation() async{
    final GeolocatorPlatform geolocatorPlatform = GeolocatorPlatform.instance;
    var position = await geolocatorPlatform.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    //var lastPosition = await geolocatorPlatform.getLastKnownPosition();
    //print(lastPosition);
    var lat = position.latitude;
    var long = position.longitude;
    setState(() {
      //locationMsg = "{$position.latitude, $position.longitude}";
      //locationMsg = "{$position}";
      locationMsg = "Latitude : $lat, Longiude : $long";
    });
  }




  void startSendingToServer() async{
    print("Sending location to the server is started !");
    /*final GeolocatorPlatform geolocatorPlatform = GeolocatorPlatform.instance;
    var position = await geolocatorPlatform.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    var lat = position.latitude;
    var long = position.longitude;*/
    var lat = 23.487561;
    var long = -5.369845;
    print("Latitude : $lat, Longiude : $long");
    var user = "Ayoub Chebab";
    /*var url = Uri.parse("https://pale-fetches.000webhostapp.com/insertdata.php");
    final response = await http.post(url, body: {
      "user": user,
      "lat": lat.toString(),
      "long": long.toString(),
    });*/
    i++;
    print(i);
    setState(() {
      timer = Timer(Duration(seconds: 2), () {
        startSendingToServer();
      });
      startBtnStatus = false;
      stopBtnStatus = true;
    });
  }



  void stopSendingToServer() async{
    print("Sending location to the server is stopped!");
    setState(() {
      timer?.cancel();
      i = 0;
      startBtnStatus = true;
      stopBtnStatus = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location Service'),
        backgroundColor: Colors.amber,
      ),
      body: Padding(
        padding: EdgeInsets.all(62.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Icon(
              Icons.location_on,
              size: 48.0,
              color: Colors.blue,
            ),
            SizedBox(height: 10.0,),
            Center(
              child: Text(
                "Get user location",
                style: TextStyle(fontSize: 26.0, fontWeight: FontWeight.bold,),
              ),
            ),
            SizedBox(height: 10.0,),
            Text("Position : $locationMsg"),
            ElevatedButton(
              onPressed: getCurrentLocation,
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.amber)),
              child: Text(
                'Get Current Location',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(height: 10.0,),
            ElevatedButton(
              onPressed: startBtnStatus?startSendingToServer: null,
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.blue)),
              child: Text(
                'Start Sending to Server',
                style: TextStyle(color: Colors.white),
              ),
            ),
            SizedBox(height: 10.0,),
            Text(
              "Sending Times : $i",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,),
            ),
            SizedBox(height: 10.0,),
            ElevatedButton(
              onPressed: stopBtnStatus?stopSendingToServer: null,
              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.redAccent)),
              child: Text(
                'Stop Sending to Server',
                style: TextStyle(color: Colors.white),
              ),
            ),



          ],
        ),
      )
    );
  }
}


